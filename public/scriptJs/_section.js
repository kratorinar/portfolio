/*
    Mettre en place une apparition de mon texte de presentation en fondu 

    ETAPE 1: faire disparaitre ma section
    ETAPE 2: faire aparaitre ma section via le scroll en js 
    ETAPE 3: faire en sorte que l'effet fasse un fondu et
            si ca rend bien faire la meme chose avec la photo
*/

const presentation = document.querySelector("#presentation");

/* window.addEventListener("scroll", () => {
    let y = window.scrollY;
    if (y > 600 && y < 1850) {
        presentation.style.animation = "apparition 1s linear forwards";
    } else if (y > 400 && largeur >= 375) {
        progress.style.animation = "apparition 1s linear forwards";
    } else {
        presentation.style.animation = "disparition 1s ";
    }
}); */

const progress = document.querySelector("#competences");

/* window.addEventListener("scroll", () => {
    let x = window.scrollY;
    if (x > 1200 && largeur > 768) {
        progress.style.animation = "apparition 1s linear forwards";
    } else if (x > 800 && largeur >= 375 && largeur <= 768) {
        progress.style.animation = "apparition 1s linear forwards";
    } else if (x > 500 && largeur <= 375) {
        progress.style.animation = "apparition 1s linear forwards";
    } else {
        progress.style.animation = "disparition 1s ";
    }
    console.log(x);
}); */

/*
    On defini les mediaquery en js
 pour pouvoir affecter mon scroll 
 on defini un debut et une fin de scroll pour eviter une boucle infini sur le scroll
*/
const projetText = document.querySelector(".projet-text");

let desktop = window.matchMedia("(min-width: 768px)");
let tablette = window.matchMedia("(max-width: 768px)");
let smartphone = window.matchMedia("(max-width: 375px)");

// function de gestion d'apparition au scroll par raport a la taille de la fenetre
function myPresentation(scroll) {
    if (scroll > 600 && scroll < 1700 && desktop.matches) {
        presentation.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 400 && scroll < 1800 && tablette.matches) {
        presentation.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 400 && scroll < 1330 && smartphone.matches) {
        presentation.style.animation = "apparition 1s linear forwards";
    } else {
        presentation.style.animation = "disparition 1s ";
    }
};

function progression(scroll) {
    if (scroll > 1200 && scroll < 2700 && desktop.matches) {
        progress.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 800 && scroll < 2150 && tablette.matches) {
        progress.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 700 && scroll < 1400 && smartphone.matches) {
        progress.style.animation = "apparition 1s linear forwards";
    } else {
        progress.style.animation = "disparition 1s ";
    }
};

function projetTexte(scroll) {
    if (scroll > 1600 && scroll < 3200 && desktop.matches) {
        projetText.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 800 && scroll < 2150 && tablette.matches) {
        projetText.style.animation = "apparition 1s linear forwards";
    } else if (scroll > 700 && scroll < 1400 && smartphone.matches) {
        projetText.style.animation = "apparition 1s linear forwards";
    } else {
        projetText.style.animation = "disparition 1s ";
    }

};

window.addEventListener("scroll", () => {
    let x = window.scrollY;
    progression(x);
    myPresentation(x);
    projetTexte(x);
    //console.log(scrollY);
});

