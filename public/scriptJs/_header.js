const nav = document.querySelector("#navbar");

window.addEventListener("scroll", () => {

  if (window.scrollY > 700) {
    nav.style.top = 0;
  } else {
    nav.style.top = "-100px";
  }
});


/*
  on veux faire une bar de scroll qui est change son apparition selon
  son width pour le mediaquery
*/
const largeur = window.innerWidth;

window.addEventListener("scroll", () => {

  if (window.scrollY > 600 && largeur >= 768) {
    nav.style.top = 0;
  } else {
    nav.style.top = "-100px";
  }
});

window.addEventListener("scroll", () => {

  if (window.scrollY > 300 && largeur >= 375) {
    nav.style.top = 0;
  } else {
    nav.style.top = "-100px";
  }
});