// toggle qui switch l'apparition de ma description
function toggleClick(tralala) {
    // On crée la classe dynamiquement
    let classe = '.figure' + tralala
    // On cible la description
    let description = document.querySelector(classe);

    // si overlay est la, on l'enleve et on met showdescription
    if (description.classList.contains("overlay")) {
        description.classList.remove("overlay")
        description.classList.add("showDescription")
    } else {
        // on enleve showdescription au deuxieme clik pour mettre overlay
        description.classList.remove("showDescription")
        description.classList.add("overlay")
    }
}
// on ecoute les button pour activer la fonction
const btn = document.querySelectorAll(".click-description");
// On fait la boucle pour écouter chaque btn
for (let i = 0; i < btn.length; i++) {
    btn[i].addEventListener("click", function () {
        // On envoie l'index du btn dans la fonction toggleClick pour cibler la bonne classe figure#
        toggleClick(i)
    });
}