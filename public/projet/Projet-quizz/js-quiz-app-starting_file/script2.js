class Question {
    constructor(text, choices, answer) {
        this.text = text;
        this.choices = choices;
        this.answer = answer;
    }
    isCorrectAnswer(choice) {
        return this.answer === choice;
    }
}
let questions = [
    new Question("Quel est le meilleure buteur de l'Histoire de Premier League ?", ["Alan Shearer", "Wayne Rooney", "Andy Cole", "Sergio Aguero"], "Alan Shearer"),
    new Question("Quel est le joueur qui à gagner le plus de Ligue des Champions ?", ["Cristiano Ronaldo", "Paolo Maldini", "Alfredo Di Stéfano", "Francisco Gento"], "Francisco Gento"),
    new Question("Quel est le meilleure buteur de l'Histoire de la Liga ?", ["Cristiano Ronaldo", "Lionel Messi", "Telmo Zarra", "Alfredo Di Stéfano"], "Lionel Messi"),
    new Question("Quel est le joueur le plus titré de l'Histoire du foot?", ["Dani Alves", "Cristiano Ronaldo", "Lionel Messi", "Andres Iniesta"], "Dani Alves"),
    new Question("Qui est le meilleure buteur de l'Histoire de Ligue des Champions ?", ["Karim Benzema", "Cristiano Ronaldo", "Lionel Messi", "Robert Lewandoski"], "Cristiano Ronaldo"),
    new Question("Quel est le meilleure buteur de l'Histoire de la Ligue 1 ?", ["Delio Onnis", "Bernard Lacombe", "Hervé Revelli", "Roger Courtois"], "Delio Onnis"),
    new Question("Quel est le club le plus titré de Ligue 1 ?", ["FC Nantes", "Paris Saint-Germain", "Olympique de Marseille", "AS Saint-Étienne"], "AS Saint-Étienne"),
    new Question("Quel est la Nation avec le plus de titre de Champion du Monde ?", ["Allemagne", "Italie", "Brésil", "France"], "Brésil"),
    new Question("Qui est le joueur qui a marqué le plus de but en Coupe du Monde ?", ["Just Fontaine", "Miroslav Klose", "Ronaldo Nazario", "Gerd Müller"], "Miroslav Klose"),
    new Question("Quel est le joueur ayant marqué trois triplé dans seule édition de Ligue des Champions ?", ["Cristiano Ronaldo", "Mario Gomez", "Lionel Messi", "Robert Lewandoski"], "Cristiano Ronaldo")
];

console.log(questions);

class Quiz {
    constructor(questions) {
        this.score = 0;
        this.questions = questions;
        this.currentQuestionIndex = 0;
    }
    getCurrentQuestion() {
        return this.questions[this.currentQuestionIndex];
    }
    guess(answer) {
        if (this.getCurrentQuestion().isCorrectAnswer(answer)) {
            this.score++;
        }
        this.currentQuestionIndex++;
    }
    hasEnded() {
        return this.currentQuestionIndex >= this.questions.length;
    }
}

// Regroup all  functions relative to the App Display
const display = {
    elementShown: function (id, text) {
        let element = document.getElementById(id);
        element.innerHTML = text;
    },
    endQuiz: function () {
        endQuizHTML = `
      <h1>Quiz terminé !</h1>
      <h3> Votre score est de : ${quiz.score} / ${quiz.questions.length}</h3>`;
        this.elementShown("quiz", endQuizHTML);
    },
    question: function () {
        this.elementShown("question", quiz.getCurrentQuestion().text);
    },
    choices: function () {
        let choices = quiz.getCurrentQuestion().choices;

        guessHandler = (id, guess) => {
            document.getElementById(id).onclick = function () {
                quiz.guess(guess);
                quizApp();
            }
        }
        // display choices and handle guess
        for (let i = 0; i < choices.length; i++) {
            this.elementShown("choice" + i, choices[i]);
            guessHandler("guess" + i, choices[i]);
        }
    },
    progress: function () {
        let currentQuestionNumber = quiz.currentQuestionIndex + 1;
        this.elementShown("progress", "Question " + currentQuestionNumber + " sur " + quiz.questions.length);
    },
};

// Game logic
quizApp = () => {
    if (quiz.hasEnded()) {
        display.endQuiz();
    } else {
        display.question();
        display.choices();
        display.progress();
    }
}
// Create Quiz
let quiz = new Quiz(questions);
quizApp();

console.log(quiz);